<?php

class Codigo
{
    public $id;
    public $codigo;
    public $nivel;
    public $usado;


    public function __construct()
    {
        $this->tabla = "com_codigos";
    }


    public function verificarCodigo($codigo, $user)
    {
        if (empty($codigo)) {
            header("Location: codigo.php?err=1");
        }
        if (empty($user)) {
            header("Location: login.php");
        }

        $db = Db::getInstance();
        $sql = "SELECT * FROM " . $this->tabla . " WHERE (codigo = :codigo and usado= :usado)  or codembajador= :codembajador LIMIT 1";
        $bind = array(
            ':usado' => 0,
            ':codigo' => $codigo,
            ':codembajador' => $codigo
        );

        $cont = $db->run($sql, $bind);
        if ($cont == 0) {
            header("Location: codigo.php?err=2");
        } else {

            $db1 = Db::getInstance();
            $row_p = $db1->fetchAll($sql, $bind);
            $nivel = $row_p[0]['nivel'];
            $codembajador = $row_p[0]['codembajador'];
            $usado = $row_p[0]['usado'];
            if ($nivel == 1 && $usado == 1 && $codembajador == null) {
                header("Location: codigo.php?err=3");
            } else {
                $db2 = Db::getInstance();
                $data = array(
                    'codigo_inscripcion' => $codigo
                );
                $db2->update('com_alumnos', $data, 'id = :id', array(':id' => $user));//todo: update info com_codigos level 1

                if ($codembajador) {

                    $new_codembajador = $this->randomCodeEmbajador($user);
                    $data = [
                        'id_alumno' => $user,
                        'codigo' => $codembajador,
                        'codembajador' => $new_codembajador,
                        'nivel' => 2,
                        'usado' => 0,

                    ];
                    Db::getInstance()->insert($this->tabla, $data);//todo: insert info com_codigos level 2
                    header("Location: residencia.php");
                } else {
                    $codembajador = $this->randomCodeEmbajador($user);
                    $data = array(
                        'usado' => 1,
                        'codembajador' => $codembajador,
                        'id_alumno' => $user
                    );
                    $db2->update('com_codigos', $data, 'codigo = :codigo', array(':codigo' => $codigo));
                    header("Location: residencia.php");
                }

            }
        }
    }


    public function randomCodeEmbajador($id)
    {
        $permitted_chars = '0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ';
        return substr(str_shuffle($permitted_chars), 0, 4) . 'OP' . $id;
    }
}
