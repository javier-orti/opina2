<?php

class Reunion
{
	public $id;
	public $categoria_esp;
	public $categoria_eng;
	public $proyecto;
	public $orden;


    public function __construct()
    {
       // echo "<p>Class X</p>";
	    $this->tabla = "com_reuniones";
	
    }
	
	private function getOrden($tabla='com_reuniones')
    {
		
				$db = Db::getInstance();
				$sql = "SELECT * FROM ".$tabla." WHERE orden > :id ORDER BY orden DESC LIMIT 1";
    			$bind = array(
        		':id' => 0
    			);
		        
				$cont = $db->run($sql, $bind);
				//echo "contador:".$cont;
				if ($cont == 0) {
					$orden = 1;
				} else {
					$db1 = Db::getInstance();
					$row_p = $db1->fetchAll($sql, $bind);
				   foreach($row_p as $row_p1) {
						$orden = $row_p1['orden'] + 1;
					}
				}
		
		return sprintf($orden);
	}
		
	public function agregar ()
    {
	   if (empty($this->categoria_esp)) {
		   header("Location: categorias_add.php");
	   } else {
			$this->orden = $this->getOrden();
			$db = Db::getInstance();
			$data = array(
        	'categoria_esp' => $this->categoria_esp,
        	'categoria_eng' => $this->categoria_eng,
			'orden' => $this->orden		
		);
    	$db->insert('com_categorias', $data);
		   
		header("Location: categorias.php");
	   }
		
    }
	
	public function modificar ()
    {
	   if (empty($this->id)) {
		   header("Location: categorias.php");
	   }
		else if (empty($this->categoria_esp)) {
		   header("Location: categorias_mod.php?id=".$this->id);
	   } else {
			
			$db = Db::getInstance();
			$data = array(
        	'categoria_esp' => $this->categoria_esp,
        	'categoria_eng' => $this->categoria_eng	
		);
    	//$db->insert('com_proyectos', $data);
		   
		   $db->update('com_categorias', $data, 'id = :id', array(':id' => $this->id));
		   
		header("Location: categorias.php");
	   }
		
    }
	
	
	public function registrarReunion ($titulo, $fecha, $id_zoom, $url_zoom, $clave)
    {
	   if (empty($titulo)) {
		   //header("Location: categorias_add.php");
		   return "err1";
	   } else {
			
			$db = Db::getInstance();
			$data = array(
        	'titulo' => $titulo,
        	'fecha' => $fecha,
			'id_zoom' => $id_zoom,
			'link_zoom' => $url_zoom,
			'clave' => $clave
			
		);
		
    	$db->insert('com_reuniones', $data);
		
		$id = $db->lastInsertId();
		
		
		
		$friendly_url = "evento-".$id;
		$db = Null;
		$db = Db::getInstance();
			$data = array(
        	'friendly_url' => $friendly_url
		);
		   
		   $db->update('com_reuniones', $data, 'id = :id', array(':id' => $id));
   		
		
		
	   }
		
    }
	
	
	public function modificarReunion ($id, $titulo, $fecha)
    {
	   if (empty($titulo)) {
		   //header("Location: categorias_add.php");
		   return "err1";
	   } else {
			
			$db = Db::getInstance();
			$data = array(
        	'titulo' => $titulo,
        	'fecha' => $fecha
			
		);
		
    	
		
		
		   
		   $db->update('com_reuniones', $data, 'id = :id', array(':id' => $id));
   		
		
		
	   }
		
    }
	
	
	
	
	
	public function getAll ($tipo = '', $limit = '')
	{
				$db = Db::getInstance();
				$sql = "SELECT com_reuniones.* FROM com_reuniones"; 
				
				$sql .= " WHERE com_reuniones.id > :id";
				$bind = array(
        		':id' => '0'
    			);				
				
				
				
					$sql .= " ORDER BY fecha DESC";
					
				
								
				/*echo $sql;
				print_r($bind);
echo "<br><br>";*/				
				
    			
		       //echo $sql;
				$cont = $db->run($sql, $bind);
				if ($cont == 0) {
					return "";
					
				} else {
					
					$db1 = Db::getInstance();
					$row_p = $db1->fetchAll($sql, $bind);
					 $conty = 0;
				   return $row_p;
				}
	}
	
	public function inscribir($evento, $usuario) {
		
		$check = Evento::verificarAsistencia($evento, $usuario);
		
		if ($check == 0) {
			$db = Db::getInstance();
			$data = array(
        	'evento' => $evento,
        	'usuario' => $usuario,
			'fecin' => date('Y-m-d H:i:s')
			
			);
			$db->insert('com_evento_registro', $data);
		}
		
		
		   
		
		
		
	}
	
	
	public function getOne ($id)
	{
				$db = Db::getInstance();
				$sql = "SELECT * FROM com_reuniones WHERE id = :id LIMIT 1";
    			$bind = array(
					':id' => $id
    			);
				
				/*echo $sql;
				print_r($bind);*/
		        
				$cont = $db->run($sql, $bind);
				if ($cont == 0) {
					return "";
				} else {
					
					$db1 = Db::getInstance();
					$row_p = $db1->fetchAll($sql, $bind);
					return $row_p;
				   
				}
	}
	
	
	public function getOnebyURL ($evento,$proximos=0)
	{
				$db = Db::getInstance();
				$sql = "SELECT * FROM ".$this->tabla." WHERE friendly_url = :url";
    			$bind = array(
					':url' => $evento
    			);
				if ($proximos == 1) {
					$date = new DateTime();
					$date->modify('-6 hours');;
					$lafechoa=  $date->format('Y-m-d H:i:s');									
									//echo "Tipo".$tipo;
									if ($tipo == 'proximos') {
										$sql .= " AND fecha >= :fecha"; 
										$bind[':fecha'] = $lafechoa;
									}
					
				}
				
				$sql .= " LIMIT 1";
				
				/*echo $sql;
				print_r($bind);*/
		        
				$cont = $db->run($sql, $bind);
				if ($cont == 0) {
					$this->row = "";
				} else {
					
					$db1 = Db::getInstance();
					$row_p = $db1->fetchAll($sql, $bind);
					$this->row = $row_p;
				   
				}
	}
	
	
	
	static function verificarAsistencia($evento, $user) {
		$db = Db::getInstance();
				$sql = "SELECT * FROM com_evento_registro WHERE evento = :evento AND usuario = :user";
				$bind = array(
        		':evento' => $evento,
				':user' => $user
    			);
				
				/*echo $sql;
				print_r($bind);*/
				$cont = $db->run($sql, $bind);
				if ($cont == 0) {
					
					//echo "no hay";
					return 0;
					
				} else {
					//echo "si hay";
					return 1;
				}
		
	}
	
	public function checkEncuesta ($evento) {
		
		$lacoo = $_COOKIE["encuesta_".$evento];
		if ($lacoo == 1) {
			return 1;
		} else {
			return 0;
		}
		
		
	}
	
	public function guardarEncuesta($p1, $p2, $p5, $p6, $alumno) {
 //echo $p1.", ".$p2.", ".$p5.", ".$p6;
					$db1 = null;
					$db1 = Db::getInstance();
					$data1 = array(
						'alumno' => $alumno,
        				'evento' => $this->row[0]['id'],
        				'p1' => $p1,
        				'p2' => $p2,
        				'p5' => $p5,
        				'p6' => $p6,
        				'fecha' => date('Y-m-d H:i:s')
					);
					//print_r($data1);
    				$db1->insert('com_evento_encuesta', $data1);
					
					setcookie("encuesta_".$this->row[0]['id'],'1',time() + 365 * 24 * 60 * 60);
					

	}
	
	public function registrarEntrada($evento,$user) {
		
		$db = Db::getInstance();
			$data = array(
        	'entrada' => '1'
		);
		   
		   $db->update('com_evento_registro', $data, 'evento = :evento AND usuario= :usuario', array(':evento' => $evento,':usuario' => $user));
		   
		
	}
	
	
	public function actualizarFotoG($valor,$id,$tipo) {
        
        $db = Db::getInstance();
			$data = array(
                            'imagen' => $valor
                        );

		   
		   $db->update('com_reuniones', $data, 'id = :id', array(':id' => $id));
        
    }
	
	
		
		
		
}