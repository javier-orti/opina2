<?php

class Alumno
{
    public $id;
    public $titulo;
    public $imagen;
    public $tabla;

    public $estado;
    public $row;

    public $pag = 1;
    public $limit = 50;
    public $orden = "";
    public $tiporden = "";
    public $total_pages;

    public $img_ppl;

    public $cnt_img_ppl;

    private $interfaz;


    public function __construct($interfaz = 0)
    {
        $this->interfaz = $interfaz;
        $this->tabla = "com_registro";
        $this->tabla2 = "com_alumnos";

    }


    public function agregar()
    {
        if (empty($this->nombre) or empty($this->email) or empty($this->pass)) {
            header("Location: usuarios_add.php");
        } else {

            $db = Db::getInstance();
            $data = array(
                'nombre' => $this->nombre,
                'email' => $this->email,
                'pass' => $this->pass,
                'sucursal' => $this->sucursal,
                'nivel' => $this->nivel
            );
            $db->insert($this->tabla, $data);
            $this->id = $db->lastInsertId();

            //header("Location: usuarios_up.php?id=".$this->id);
            header("Location: usuarios.php");
        }

    }


    public function modificar()
    {
        if (empty($this->id)) {
            header("Location: usuarios.php");
        } else if (empty($this->email)) {
            header("Location: usuarios_mod.php?id=" . $this->id);
        } else {

            $db = Db::getInstance();
            $data = array(
                'nombre' => $this->nombre,
                'email' => $this->email,
                'sucursal' => $this->sucursal,
                'nivel' => $this->nivel
            );
            //$db->insert('com_proyectos', $data);

            $db->update($this->tabla, $data, 'id = :id', array(':id' => $this->id));

            header("Location: usuarios.php");
        }

    }

    public function addResidencia($ano, $user)
    {
        if (empty($ano)) {
            header("Location: residencia.php?err=1");
        } else if (empty($user)) {
            header("Location: login.php");
        } else {
            $db = Db::getInstance();
            $data = array(
                'ano_residencia' => $ano
            );
            $db->update($this->tabla2, $data, 'id = :id', array(':id' => $user));
            header("Location: index.php");
        }

    }

    public function modificarPass()
    {
        if (empty($this->id)) {
            header("Location: usuarios.php");
        } else if (empty($this->pass)) {
            header("Location: usuarios_mod.php?id=" . $this->id);
        } else {

            $db = Db::getInstance();
            $data = array(
                'pass' => $this->pass
            );
            //$db->insert('com_proyectos', $data);

            $db->update($this->tabla, $data, 'id = :id', array(':id' => $this->id));

            //header("Location: usuarios.php");
        }

    }


    public function getAll($paginado = 1, $tipo = 'todos', $tipoLimit = '', $externo = 0, $opciones = array())
    {

        $db = Db::getInstance();

        $sql = "SELECT " . $this->tabla . ".* FROM " . $this->tabla . " ";

        $sql .= "WHERE " . $this->tabla . ".id > :id";
        $bind = array(
            ':id' => '0'
        );


        $sql .= " AND " . $this->tabla . ".activado=1";


        if (!empty($opciones['nombre'])) {
            $nombre = $opciones['nombre'];
            $nombre = str_replace(", ", ",", $nombre);
            $nombre = str_replace(",", " ", $nombre);
            $nombres = explode(" ", $nombre);
            $concatenador = "AND ";
            $conti = 1;

            foreach ($nombres as $word) {
                //if ($conti >1){
                $sql .= " " . $concatenador;
                //    }
                $sql .= " (nombre LIKE :nombre_" . $conti . " OR ape1 LIKE :nombre_" . $conti . " OR ape2 LIKE :nombre_" . $conti . ")";
                $bind[":nombre_" . $conti] = "%$word%";
                $conti++;
            }

        }

        if (!empty($opciones['email'])) {
            $sql .= " AND " . $this->tabla . ".email = :email";
            $bind[":email"] = $opciones['email'];
        }


        if (empty($this->orden)) {
            $orden = $this->tabla . ".ape1, " . $this->tabla . ".nombre";
        } else {
            $orden = $this->orden;
        }


        if ($this->tiporden == 'desc') {
            $tiporden = " desc";
        } else {
            $tiporden = "";
        }

        /*   echo $sql;
         print_r($bind);
         echo "<br><br>";*/


        if ($paginado == 1) {


            $total_results = $db->run($sql, $bind);
            $this->total_results = $total_results;
            $total_pages = ceil($total_results / $this->limit);
            $this->total_pages = $total_pages;


            $starting_limit = ($this->pag - 1) * $this->limit;


            $sql .= " ORDER BY " . $orden . $tiporden . " LIMIT " . $starting_limit . "," . $this->limit;
        } else {
            $sql .= " ORDER BY " . $orden . $tiporden;
        }


        $cont = $db->run($sql, $bind);
        if ($cont == 0) {
            $row_p = "";
        } else {

            $db1 = Db::getInstance();
            $row_p = $db1->fetchAll($sql, $bind);
            $conty = 0;
            foreach ($row_p as $row_p1) {
                $conty++;
            }
            $this->row = $row_p;
        }
    }


    public function getOne($id)
    {
        $db = Db::getInstance();
        $sql = "SELECT * FROM " . $this->tabla . " WHERE id = :id LIMIT 1";
        $bind = array(
            ':id' => $id
        );

        $cont = $db->run($sql, $bind);
        if ($cont == 0) {
            $row_p = "";
        } else {

            $db1 = Db::getInstance();
            $row_p = $db1->fetchAll($sql, $bind);

            $this->row = $row_p;

        }
    }


}