<?php

require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';

include("function_api.php");


$url = 'https://www.esteve.es/EsteveFront/Login.do?op=LEX';


$fields = array();

$fields['usu_email'] = datoApi($usu_email);
$fields['usu_password'] = datoApi($usu_password);



foreach ($fields as $key => $value) {
	$fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');

$ch = curl_init($url);
# Form data string
$postString = http_build_query($data);

//echo $postString;
# Setting our options
curl_setopt($ch, CURLOPT_POST, count($fields));

curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
# Get the response
$response = curl_exec($ch);
curl_close($ch);

//echo $response;

//die();
if (strpos($_SERVER['HTTP_REFERER'], '?') !== false) {
	$symbolU = "&";
} else {
	$symbolU = "?";
}
if ($response == 'KO' or empty($response)) {
	//echo $response;
	header("Location: " . $_SERVER['HTTP_REFERER'] . $symbolU . "err=2");
} else {
	//print_r($response);
	//die();
	$campos = array();
	$datos = explode(";", $response);
	foreach ($datos as $element) {
		$datos1 = explode("=", $element);
		$campos[$datos1[0]] = utf8_encode($datos1[1]);
	}
	// aqui verificamos si el usuario está registrado en nuestra web

	$dbff = Db::getInstance();
	$sqlff = "SELECT * FROM com_alumnos WHERE codusuario = :codusuario";
	$bindff = array(
		':codusuario' => $campos['usu_codusuario']
	);

	$contff = $dbff->run($sqlff, $bindff);



	if (empty($_COOKIE["clave"])) {
		$clave00 = uniqid();
	} else {
		$clave00 = $_COOKIE["clave"];
	}
	$clave02 = createhash($tokensArray, $hashLenght);

	if ($contff > 0) {
		$db1 = null;
		$db1 = Db::getInstance();
		$rowff = $db1->fetchRow($sqlff, $bindff);

		$db = Db::getInstance();
		$data = array(
			'ape1' => $campos['usu_ape1'],
			'ape2' => $campos['usu_ape2'],
			'email' => $campos['usu_email'],
			'nombre' => $campos['usu_nombre'],
			'dni' => $campos['usu_dni'],
			'perfil' => $campos['usu_perfil'],
			'especialidad' => $campos['usu_especialidad'],
			'numcolegiado' => $campos['usu_numcolegiado'],
			'pais' => $campos['usu_pais'],
			'provincia' => $campos['usu_provincia'],
			'poblacion' => $campos['usu_poblacion'],
			'ciudad' => $campos['usu_ciudad'],
			'direccion' => $campos['usu_direccion'],
			'cp' => $campos['usu_cp'],
			'telefono' => $campos['usu_telefono'],
			'fax' => $campos['usu_fax'],
			'empresa' => $campos['usu_empresa'],
			'usu_tipo' => $campos['usu_tipo'],
			'clave' => $clave00,
			'fecha' => $fechoy,
			'pass' => $clave02

		);
		$db->update('com_alumnos', $data, 'codusuario = :codusuario', array(':codusuario' => $campos['usu_codusuario']));
		$db = null;


		$db = Db::getInstance();
		$data = array(
			'user' => $rowff['id']
		);
		$db->update('com_log', $data, 'sesion = :clave00', array(':clave00' => $clave00));
		$db = null;
	} else {
		// registramos al usuario si no existe

		$db = Db::getInstance();
		$data = array(
			'ape1' => $campos['usu_ape1'],
			'ape2' => $campos['usu_ape2'],
			'codusuario' => $campos['usu_codusuario'],
			'email' => $campos['usu_email'],
			'nombre' => $campos['usu_nombre'],
			'dni' => $campos['usu_dni'],
			'perfil' => $campos['usu_perfil'],
			'especialidad' => $campos['usu_especialidad'],
			'numcolegiado' => $campos['usu_numcolegiado'],
			'pais' => $campos['usu_pais'],
			'provincia' => $campos['usu_provincia'],
			'poblacion' => $campos['usu_poblacion'],
			'ciudad' => $campos['usu_ciudad'],
			'direccion' => $campos['usu_direccion'],
			'cp' => $campos['usu_cp'],
			'telefono' => $campos['usu_telefono'],
			'fax' => $campos['usu_fax'],
			'empresa' => $campos['usu_empresa'],
			'usu_tipo' => $campos['usu_tipo'],
			'clave' => $clave00,
			'fecha' => $fechoy,
			'pass' => $clave02
		);
		$db->insert('com_alumnos', $data);
		$ida = $db->lastInsertId();
		$db = null;



		$db = Db::getInstance();
		$data = array(
			'user' => $ida
		);
		$db->update('com_log', $data, 'sesion = :clave00', array(':clave00' => $clave00));
		$db = null;
	}



	$dbff = Db::getInstance();
	$sqlff = "SELECT * FROM com_alumnos WHERE codusuario = :codusuario";
	$bindff = array(
		':codusuario' => $campos['usu_codusuario']
	);

	$contff = $dbff->run($sqlff, $bindff);


	if ($contff > 0) {
		$db1 = null;
		$db1 = Db::getInstance();
		$rowff = $db1->fetchRow($sqlff, $bindff);

		setcookie("codusuario_jko", $rowff['codusuario']);
		setcookie("id_jko", $rowff['id']);

		if (empty($_COOKIE["clave"])) {
			$clave00 = uniqid();
		} else {
			$clave00 = $_COOKIE["clave"];
		}


		if ($rowff['servicio'] == 1) {
			$clave00 = uniqid();
			setcookie("clave", $clave00);
			setcookie("servicio", '1');

			$db = Db::getInstance();
			$data = array(
				'clave' => $clave00
			);
			$db->update('com_alumnos', $data, 'codusuario = :codusuario', array(':codusuario' => $rowff['codusuario']));
			$db = null;


			//echo "esta el usuario y está registrado en el servicio";
			header("Location: index.php");
		} else {

			//echo "aqui revisa";
			// aqui revisamos si está inscrito en el servicio
			$urls = 'http://www.esteve.es/EsteveFront/Inscripcion.do';



			$fieldss = array();

			$fieldss['usu_codusuario'] = $rowff['codusuario'];
			$fieldss['zon'] = $zon_ppal;
			$fieldss['con'] = $con_ppal;


			foreach ($fieldss as $key => $value) {
				$fieldss_string .= $key . '=' . $value . '&';
			}
			rtrim($fieldss_string, '&');

			$chs = curl_init($urls);
			# Form data string
			$postStrings = http_build_query($datas);
			//$postString = "usu_email=gianna@tba.es";
			//echo $postString;
			# Setting our options
			curl_setopt($chs, CURLOPT_POST, count($fieldss));

			curl_setopt($chs, CURLOPT_POSTFIELDS, $fieldss_string);
			curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
			# Get the response
			$responses = curl_exec($chs);
			curl_close($chs);

			//echo "<br>".$responses;
			if ($responses == 'false') {

				// 	AQUI EMPIEZA EL REGISTRO EN EL SERVICIO

				$url = 'http://www.esteve.es/EsteveFront/Service.do?op=AIX';
				$dev_OK = 'index.php?status=OK';
				$dev_KO = 'registro_service.php?status=KO';

				$fields = array();

				$fields['con'] = datoApi($con_ppal);
				$fields['zon'] = datoApi($zon_ppal);
				$fields['usu_codusuario'] = datoApi($rowff['codusuario']);

				$fields['usu_nombre'] = datoApi($rowff['nombre']);
				$fields['usu_ape1'] = datoApi($rowff['ape1']);
				$fields['usu_ape2'] = datoApi($rowff['ape2']);
				$fields['usu_email'] = datoApi($rowff['email']);
				$fields['usu_codperfil'] = datoApi($rowff['perfil']);
				if (empty($rowff['especialidad'])) {
					$fields['usu_codespecialidad'] = 0;
				} else {
					$fields['usu_codespecialidad'] = datoApi($rowff['especialidad']);
				}

				$fields['usu_codpais'] = datoApi($rowff['pais']);
				$fields['usu_codprovestado'] = datoApi($rowff['provincia']);


				$response =  datosApi($url, $fields);

				//echo $response;

				if ($response == "KO" or empty($response)) {
					header("Location: " . $dev_KO);
				} else {
					$clave00 = uniqid();
					setcookie("clave", $clave00);

					$db = Db::getInstance();
					$data = array(
						'clave' => $clave00,
						'servicio' => '1'

					);
					$db->update('com_alumnos', $data, 'codusuario = :codusuario', array(':codusuario' => $rowff['codusuario']));

					setcookie("servicio", '1');
					header("Location: " . $dev_OK);
				}



				// AQUI TERMINA EL REGISTRO EN EL SERVICIO






			} else {
				$clave00 = uniqid();
				setcookie("clave", $clave00);
				setcookie("servicio", '1');
				$db = Db::getInstance();
				$data = array(
					'clave' => $clave00,
					'servicio' => '1'
				);
				$db->update('com_alumnos', $data, 'codusuario = :codusuario', array(':codusuario' => $campos['usu_codusuario']));
				$db = null;

				header("Location: index.php");
			}
			// terminamos de revisar si está inscrito en el servicio 
			//echo "esta el usuario pero no está registrado en el servicio";

		}
	}

	//aqui termina el if de si el resultado de la consulta a la api fue positivo o no
}
//echo $response;
