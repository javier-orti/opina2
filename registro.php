<?php
$err = '';
require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';
$page = 'registro';

?>

<?php if ($err == '6') { ?>
    <div class="alert alert-danger">El email que está intentado registrar ya está registrado en la base de datos, use su
        correo y contraseña para acceder o recupere su contraseña si la ha olvidado.
    </div>
<?php } ?>
<section id="register-alumno">
    <div class="container p-5">
        <div class="text-center letter-red"><p>Regístrate para acceder</p></div>
        <form action="action_registro.php?action=registro" method="post" id="registro">
            <input type="hidden" name="con" value="<?php echo $con_ppal ?>">
            <input type="hidden" name="zon" value="<?php echo $zon_ppal ?>">
            <div class="row row-cols-1">
                <div class="col-12">
                    <input type="email" class="form-control" name="usu_email" placeholder="Correo electrónico*"
                           autocomplete="new-email" required/>
                </div>
                <div class="col-lg-6 mt-3">
                    <input type="password" class="form-control" name="usu_password" id="usu_password"
                           placeholder="Contraseña*"
                           autocomplete="new-password" required/>
                </div>
                <div class="col-lg-6 mt-3">
                    <input type="password" class="form-control" name="usu_password2" id="usu_password2"
                           placeholder="Repetir contraseña*" autocomplete="new-password" required/>
                </div>

            </div>
            <div class="row row-cols-1">
                <div class="col-12 mt-3">
                    <input type="text" class="form-control" name="usu_nombre" placeholder="Nombre*" required/>
                </div>
                <div class="col-lg-6 mt-3">
                    <input type="text" class="form-control" name="usu_ape1" placeholder="Primer apellido*" required/>
                </div>
                <div class="col-lg-6 mt-3">
                    <input type="text" class="form-control" name="usu_ape2" placeholder="Segundo apellido*" required/>
                </div>

                <div class="col-lg-6 mt-3">
                    <input type="text" class="form-control" name="usu_dni" placeholder="NIF / NIE">
                </div>

                <div class="col-lg-6 mt-3">
                    <select name="usu_codperfil" id="usu_codperfil" class="form-control">
                        <option value="0">Eres*</option>
                        <option value="ME">Médico</option>
                        <option value="FA">Farmacéutico</option>
                        <option value="AX">Auxiliar de Farmacia</option>
                        <option value="OT">Otros</option>
                        <option value="RS">Residente</option>
                        <option value="EN">Enfermero</option>
                    </select>
                </div>

                <div class="col-lg-12 mt-3">
                    <input type="text" class="form-control" name="usu_empresa" placeholder="Centro de trabajo">
                </div>
                <div class="col-lg-6 mt-3">
                    <select class="form-control" name="usu_codpais" id="usu_codpais" autocomplete="off">
                        <option value="">Pais*</option>
                        <?php
                        $db_pais = Db::getInstance();
                        $sql_pais = "SELECT * FROM com_paises ORDER BY pais";
                        $bind_pais = array(
                            ':codusuario' => ''
                        );
                        $row_pais1 = $db_pais->fetchAll($sql_pais);
                        foreach ($row_pais1 as $row_pais) {
                            ?>
                            <option value="<?php echo $row_pais['codigo']; ?>"><?php echo $row_pais['pais']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-6 mt-3" id="dv_provincia">
                    <select class="form-control" name="usu_codprovestado" value="" id="usu_codprovestado"
                            autocomplete="off"
                            disabled>
                        <option value="">Provincia*</option>
                    </select>
                </div>
                <div class="col-lg-12 mt-3">
                    <input class="form-control" name="usu_direccion" placeholder="Dirección">
                </div>
                <div class="col-lg-6 mt-3">
                    <input class="form-control" name="usu_cp" placeholder="Código postal">
                </div>
                <div class="col-lg-6 mt-3">
                    <input class="form-control" name="usu_telefono" placeholder="Teléfono">
                </div>

                <div class="col-md-12">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="" name="acepto" required>
                            <p style="margin-top: 25px;" class="letter-grey">He leído y
                                acepto el <a
                                        href="https://www.esteveagora.com/aviso-legal" target="_blank"
                                        title="Aviso Legal">aviso
                                    legal</a> y la <a href="https://www.esteveagora.com/privacidad" target="_blank"
                                                      title="Política de privacidad" class="info-privacy">política de
                                    privacidad</a>.</p>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="mailing" value="S">
                            <p style="margin-top: 25px;" class="letter-grey">Deseo recibir <a
                                        href="https://www.esteveagora.com/privacidad" target="_blank" title="">comunicaciones
                                    comerciales</a>.</p>
                        </label>
                    </div>
                </div>

                <div class="col text-right">
                    <!--                    <button type="submit" class="btn-main"><img src="img/flecha.png"/> CREAR CUENTA</button>-->
                    <button class="btn btn-danger" type="submit" id="btn-user-new"><i
                                class="bi bi-arrow-right-circle"></i> CREAR CUENTA
                </div>
            </div>
        </form>
    </div>
</section>
<script src="js/jquery.validate.js"></script>
<script src="js/registro.js"></script>
