<?php
$err = "";
$status = "";
require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';
$page = 'login';

?>
<?php if ($err == 1) { ?>
    <div class="alert alert-danger">Hubo un error en el registro</div>
<?php } else if ($status == 'OK') {
    include 'header.php'; ?>
    <div class="alert alert-success">Se ha enviado a su email las instrucciones para restablecer
        el password.
    </div>
<?php } ?>
<div class="container infologin p-5">

    <p class="letter-grey">Si has olvidado tu contraseña escribe tu correo electrónico y recibirás las instrucciones
        para cambiarla.</p>

    <form action="action_registro.php?action=forgot" method="post">
        <input type="hidden" name="servicio" value="<?php echo $servicio; ?>">
        <diw class="row row-cols-1 row-cols-md-2">
            <div class="col">
                <p>Correo electrónico</p>
                <input type="email" class="form-control" name="usu_email" placeholder=""
                       required>
            </div>
        </diw>
        <div class="text-right">
            <button class="btn btn-danger mb-3 pl-4 pr-4"
                    type="submit" id="btn-recover-pass"><i class="bi bi-arrow-right-circle"></i> RECUPERAR
                CONTRASEÑA
            </button>
        </div>
        <hr>
        <div class="text-left">

            <a href="login.php" class="btn-login" id="btn-init-session">
                <i class="bi bi-arrow-right-circle"></i> INICIAR SESION
            </a>
            <a href="#" class="btn btn-light pl-4 pr-4" id="btn-new-user">
                <i class="bi bi-arrow-right-circle"></i> CREAR CUENTA
            </a>
        </div>
    </form>
</div>
<script src="js/contador.js"></script>
<script src="js/main.js"></script>



