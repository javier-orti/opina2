<?php
error_reporting(E_ALL);
ini_set('display_errors', '0');
require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';
require_once 'lib/auth_off.php';
$page = 'home';
session_start();


if (!$authj->rowff['codigo_inscripcion'] && (!$_SESSION['code'] || empty($_SESSION['code']))) {
    header('location: codigo.php');
} else if (isset($authj->rowff['codigo_inscripcion']) && !$authj->rowff['ano_residencia']) {
    header('location: residencia.php');
} else {
    if (!$authj->rowff['ano_residencia']) {
        header('location: verificar_codigo.php?codigo_inscripcion=' . $_SESSION['code']) . '&user=' . $authj->rowff['id'];
    }
    $SQL = 'SELECT * FROM com_codigos where id_alumno=?'; //TODO: search info codigos user
    $result = Db::getInstance()->prepare($SQL);
    $result->bindParam(1, $authj->rowff['id']);
    $result->execute();
    $infoUser = $result->fetch();
}

include('header.php');
?>

    <div class="container-fluid">
        <div class="my-4">
            <div class="title text-center">
                <?php
                if ($authj->logueado == 1) {

                    include "all/home.php";
                    if(isset($_GET['success'])){
                        if(!checkForm_action($authj->rowff['id'])){
                            addInfoQuestionnaire($authj->rowff['id']);//todo save answers for questions for each user alumno
                        }
                    }
                    include 'all/info.php';
                }
                ?>
            </div>
        </div>
    </div>



<?php include('footer.php'); ?>


