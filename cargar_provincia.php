<?php  //ini_set('display_errors', '1');
require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';

?>
<?php
$db_pais = Db::getInstance();
$sql_pais = "SELECT * FROM com_provincias WHERE pais = :pais ORDER BY provincia";
$bind_pais = array(
  ':pais' => $pais
);
$cant_paises = $db_pais->run($sql_pais, $bind_pais);
$row_pais1 = $db_pais->fetchAll($sql_pais, $bind_pais);

?>

<select class="form-control" name="usu_codprovestado" id="usu_codprovestado" autocomplete="off" <?php if ($cant_paises == 0) { ?> disabled<?php } ?>>
  <option value="">Provincia*</option>
  <?php

  foreach ($row_pais1 as $row_pais) {
  ?>
    <option value="<?php echo $row_pais['codigo']; ?>"><?php echo $row_pais['provincia']; ?></option>
  <?php } ?>
</select>