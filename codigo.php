<?php
error_reporting(E_ALL);
ini_set('display_errors', '0');
require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';
require_once 'lib/auth.php';
$page = 'codigo';
include('header.php');
?>
<div class="contenido">
    <div class="container">
        <div class="my-4">
            <div class="title text-center">
                <p class="letter-red">Introduce tu código:</p>
                <form action="verificar_codigo.php" method="post">
                    <input id="codigo" type="text" name="codigo_inscripcion" class="form-control my-4 "
                           style="max-width:400px;margin:0 auto"
                           placeholder="Introduce aquí tu código " required>
                    <div class="text-center">
                        <button type="submit" class="pr-5 pl-5 pt-2 pb-2" id="btn-code-verificate">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>


