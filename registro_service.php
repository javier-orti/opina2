<?php /*error_reporting(E_ALL);
ini_set('display_errors', '1');*/

 require_once 'lib/autoloader.class.php';
    require_once 'lib/init.class.php';

    require_once 'lib/auth_off.php';
    $page = 'registro';
 


?>
<!doctype html>

<html lang="es">

  <head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="css/estilos.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <title>ACCESO | Continuidad Asistencial en paciente con COVID-19</title>

  </head>

  <body>

    <div class="app">

        

        <div class="main">

            <div class="topbar">

                <div class="contenedor">

                    <div class="logo">

                        <img src="img/logo.png" alt="logo">

                    </div>

                </div>

            </div>

            <div class="registro">

              <div class="contenedor">  

                  <p class="titulo">COMPRUEBE SUS DATOS</p>

                  <div class="brdcolor1 caja">

                   

                    <p><b>Área de acceso restringido</b>: Este área contiene información dirigida exclusivamente a profesionales sanitarios.</p>

                    <p class="color1"><b>CLAVES DE ACCESO A LOS SERVICIOS RESTRINGIDOS</b></p>

                     <?php if ($err=='6') { ?>
          <div class="alert alert-danger">El email que está intentado registrar ya está registrado en la base de datos, use su correo y contraseña para acceder o recupere su contraseña si la ha olvidado.</div>
          <?php } else  if ($err=='1') { ?>
            <div class="alert alert-danger">Hubo un error en el registro.</div>
          <?php } ?>

                    <form id="registro_s" action="action_registro.php?action=registro_servicio" method="post">
                      <input type="hidden" name="con" value="<?php echo $con_ppal?>">
                      <input type="hidden" name="zon" value="<?php echo $zon_ppal?>">
                      <input type="hidden" name="usu_codusuario" value="<?php echo $authj->rowff['codusuario']?>">

                    <div class="row row-cols-1">

                      <div class="col-12">

                        <input type="email" class="form-control" name="usu_email" value="<?php echo $authj->rowff['email']?>" placeholder="Correo electrónico" autocomplete="new-email" required readonly/>

                      </div>

                      

                     

                    </div>

                    <p class="color1"><b>DATOS PERSONALES</b></p>

                    <div class="row row-cols-1">

                      <div class="col-12">

                        <input type="text" class="form-control" name="usu_nombre" placeholder="Nombre*" value="<?php echo $authj->rowff['nombre']?>" required/>

                      </div>

                      <div class="col-lg-6">

                        <input type="text" class="form-control" name="usu_ape1" placeholder="Primer apellido*" value="<?php echo $authj->rowff['ape1']?>" required/> 

                      </div>

                      <div class="col-lg-6">

                        <input type="text" class="form-control" name="usu_ape2" placeholder="Segundo apellido" value="<?php echo $authj->rowff['ape2']?>" required/> 

                      </div>

                      <div class="col-lg-6">

                        <select name="usu_codperfil" id="usu_codperfil" class="form-control">
                                <option value="0">Eres</option>
                                 <option value="ME"<?php if ('ME' == $authj->rowff['perfil']) { ?> selected<?php } ?>>Médico</option>

        <option value="FA"<?php if ('FA' == $authj->rowff['perfil']) { ?> selected<?php } ?>>Farmacéutico</option>

        <option value="AX"<?php if ('AX' == $authj->rowff['perfil']) { ?> selected<?php } ?>>Auxiliar de Farmacia</option>

        <option value="OT"<?php if ('OT' == $authj->rowff['perfil']) { ?> selected<?php } ?>>Otros</option>

        <option value="RS"<?php if ('RS' == $authj->rowff['perfil']) { ?> selected<?php } ?>>Residente</option>

        <option value="EN"<?php if ('EN' == $authj->rowff['perfil']) { ?> selected<?php } ?>>Enfermero</option>
                        </select>

                      </div>

                      <div class="col-lg-6">

                      <select class="form-control" name="usu_codespecialidad" id="usu_codespecialidad" data-validation="required"<?php if ('ME' == $authj->rowff['perfil']) { } else { ?> disabled="disabled"<?php } ?> autocomplete="off">
                <option value="">Especialidad</option>
                 <?php
                          $db_pais = Db::getInstance();
                        $sql_pais = "SELECT * FROM com_especialidades ORDER BY especialidad";
                         $cont = $db_pais->run($sql_pais);
                         if ($cont > 0) {
                          $db_pais1 = Db::getInstance();
                          $row_pais1 = $db_pais1->fetchAll($sql_pais);
                          foreach($row_pais1 as $row_pais) {
                          ?>
                              <option value="<?php echo $row_pais['id'];?>"<?php if ($row_pais['id'] == $authj->rowff['especialidad']) { ?> selected<?php } ?>><?php echo $row_pais['especialidad'];?></option>
                          <?php }
                          }
                         
                          ?>
                </select>

                      </div>

                      <div class="col-lg-6">

                        <select class="form-control" name="usu_codpais" id="usu_codpais" autocomplete="off">
                          <option value="">Pais</option>
                          <?php 
                          $db_pais = Null;
                          $db_pais1 = Null;


                           $db_pais = Db::getInstance();
                         $sql_pais = "SELECT * FROM com_paises ORDER BY pais";
                         $cont = $db_pais->run($sql_pais);
                         if ($cont > 0) {
                          $db_pais1 = Db::getInstance();
                          $row_pais1 = $db_pais1->fetchAll($sql_pais);
                          foreach($row_pais1 as $row_pais) {


                      ?>
                          <option value="<?php echo $row_pais['codigo'];?>"<?php if ($row_pais['codigo'] == $authj->rowff['pais']) { ?> selected<?php } ?>><?php echo $row_pais['pais'];?></option>
                      <?php } 
                        }
                       ?>
                        </select>

                      </div>

                      <div class="col-lg-6" id="dv_provincia">

                      <select class="form-control" name="usu_codprovestado" value="" id="usu_codprovestado" autocomplete="off" disabled >
               <?php

               $db_pais = Null;
                          $db_pais1 = Null;

          $db_pais = Db::getInstance();
        $sql_pais = "SELECT * FROM com_provincias WHERE pais = :pais ORDER BY provincia";
        $bind_pais = array(
                ':pais' => $authj->rowff['pais']
            );
         $cont = $db_pais->run($sql_pais, $bind_pais);
         if ($cont > 0) {
          $db_pais1 = Db::getInstance();
          $row_pais1 = $db_pais1->fetchAll($sql_pais, $bind_pais);
          foreach($row_pais1 as $row_pais) {
          ?>
              <option value="<?php echo $row_pais['codigo'];?>"<?php if ($row_pais['codigo'] == $authj->rowff['provincia']) { ?> selected<?php } ?>><?php echo $row_pais['provincia'];?></option>
          <?php }
          } else { ?>


              <option value="">Seleccionar Provincia*</option>

              <?php }
          $db_pais =null;
          $db_pais1 =null;?> </select>

                      </div>

                      <div class="col">

                        <br>

                        <div class="check">

                          <input type="checkbox" name="acepto" id="acepto" class="css-checkbox">

                          <label for="acepto" class="css-label">He leído y acepto el <a href="https://www.esteve.com/es/avisolegal" target="_blank" title="Aviso Legal">aviso legal</a> y la <a href="https://www.esteve.com/es/privacidad" target="_blank" title="Política de privacidad" class="info-privacy">política de privacidad</a>.</label>

                        </div>

                        <div class="check">

                          <input type="checkbox" name="mailing" id="mailing" class="css-checkbox" value="S">

                          <label for="mailing" class="css-label">Deseo recibir <a href="https://www.esteve.com/es/privacidad" target="_blank" title="">comunicaciones comerciales</a>.</label>

                        </div>

                      </div>

                      <div class="col text-right">

                        <button type="submit" class="btn-main"><img src="img/flecha.png"/> ACCEDER</button>



                      </div>

                      

                    </div>

                    



                    </form>

                  </div>

              </div>

            </div>



            

            

        </div>

<?php include('footer.php');?>

    </div>



    <!-- Optional JavaScript -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

     <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

      <?php if ($page = "registro") {?>  
        <script src="js/jquery.validate.js"></script>
        <script src="js/registro.js"></script>
      <?php } ?>

  </body>

</html>