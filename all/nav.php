<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top p-0 pt-2 pb-2">
    <a href="#"><img src="img/esteve-top.png" alt="proyecto-opina-logo" id="logo-esteve"></a>
    <div class="container-fluid text-right" id="pre-nav">
        <?php
        if ($authj->logueado == 1) {
            ?>
            ?>
            <div class="text-right">

                <h6 class="pt-2 mr-4"><a href="#" style="color: white;" id="info-user"><?php

                        echo '<b>Dr. ' . $authj->rowff['nombre'] . " " . $authj->rowff['ape1'] . " " . $authj->rowff['ape2'] . '</b>'; ?>
                        <a class="text-white small" title="Cerrar sesión" href="salir.php"> <span
                                    class="material-icons pr-2" style="vertical-align:sub"> exit_to_app</span></a>
                </h6>
            </div>
            <?php
        }
        ?>
    </div>

    <?php if ($authj->logueado == 1) {


        ?>
        <button class="navbar-toggler mt-4 mr-3" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto my-2 my-lg-0 navbar-nav-scroll text-center pt-4 mr-5">
                <li class="nav-item ">
                    <a class="nav-link active" href="/opina2" id="view-home">HOME<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="/opina2#block-home-2" id="login-user">BIBLIOGRAFÍA</a>
                </li>
                <li class="nav-item">

                    <?php
                    include 'action/questionnaire-action.php';
                    if (!checkForm_action($infoUser['id_alumno'])) {
                        ?>
                        <a class="nav-link questionnaire" href="#" id="questionnaire">CUESTIONARIO</a>
                    <?php } ?>

                </li>
                <li class="nav-item">
                    <form method="get" name="invita" action="?id=<?php echo $infoUser['id_alumno']?>">
                    <a class="nav-link " href="#" id="info-invitar" >INVITAR</a>
                    </form>
                </li>
                <?php if ($infoUser['nivel'] == 2) { ?>
                    <li class="nav-item">
                        <a class="nav-link " href="#">BASES DEL SORTEO</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }


    ?>
</nav>

<div class="banner py-2">
    <div class="container">
        <div class="row row-cols-1 row-cols-lg-2">
            <div class="col">
                <img src="img/banner-logo.png" class="w-100 p-5 img-responsive" alt="proyecto-opina-logo">
            </div>
            <div class="col mt-5">
                <img src="img/banner-draw.png" class="w-75 mt-5 img-responsive" alt="proyecto-opina-dibujo">
            </div>
        </div>
    </div>
</div>



