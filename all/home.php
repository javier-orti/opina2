<div hidden id="id_user"><?php echo $infoUser['id_alumno']; ?></div>
<section id="block-home">
    <div class="container">
        <div class="container-fluid mt-4">
            <?php
            if (countInvitation($infoUser['codembajador']) > 0) {
                ?>
                <div class="container mt-3 mb-3">
                    <button class="btn btn-danger btn-block btn-users-invitation p-2">
                        <?php echo countInvitation($infoUser['codembajador']); ?> de tus invitados han respondido al
                        cuestionario
                        OpinA
                    </button>
                </div>
                <?php
            }
            ?>
            <div class="text-center"><p class="letter-red">Bienvenido</p></div>
            <div class="text-center p-4">
                <p class="letter-grey">Está ampliamente demostrada la importancia del manejo adecuado
                    de las dislipidemias tanto en prevención primaria como en secundaria y su impacto en el
                    desarrollo
                    de
                    las Enfermedades Cardiovasculares. Resulta fundamental que los profesionales de la salud
                    implicados
                    en
                    el manejo de estos pacientes adopten las prácticas más adecuadas basándose en la actualidad
                    científica y
                    el perfil del paciente con el fin de mejorar la calidad de la atención sanitaria.</p>
                <p class="letter-grey">
                    El objetivo de este proyecto es evaluar el nivel de conocimiento por parte de Endocrinos y
                    especialistas
                    en Medicina Interna de las últimas evidencias y actualizaciones en el tratamiento del
                    paciente
                    dislipidémico.</p>
            </div>
        </div>
        <div class="container-fluid  block2">
            <div class="text-center p-3">
                <p class="letter-red">¿Cómo participar?</p>
                <div class="row mt-5  mb-3">
                    <div class="col-md-4 ">
                        <a href="#"><img src="img/Componente%2034%20–%201.svg">
                            <br><br>
                            <p class="letter-grey">Lee los <b>3 artículos</b></p>
                        </a>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="#"><img src="img/Componente%2035%20–%201.svg">
                            <br><br>
                            <p class="letter-grey"><b>Danos tu opinión</b></p>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#"><img src="img/Componente%2036%20–%201.svg">
                            <br><br>
                            <?php if ($infoUser['nivel'] == 1) { ?>
                                <p class="letter-grey">Al finalizar el cuestionario <b>Invita</b> a un mínimo de <b>3
                                        colegas adjuntos</b></p>
                            <?php } else { ?>
                                <p class="letter-grey">Al finalizar el cuestionario <b>Invita a más colegas</b>
                                    adjuntos</b></p>
                            <?php } ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="block-home-2">
            <p class="letter-red text-center mt-5">Lecturas para responder cuestionario</p>
            <div class="container mt-4">
                <p class="text-center letter-grey">Te invitamos a consultar los siguientes artículos para
                    posteriormente
                    dar tu opinión.<br> Cada uno se
                    acompaña de un visual abstract que te facilitará la revisión de los contenidos más
                    destacados.
                </p>
            </div>
            <div class="m-5">
                <div class="row m-5">
                    <div class="col-md-4 m-0">
                        <a href="#">
                            <img src="img/cuadri.png">
                        </a>
                        <p class="mt-3"><span>TÍTULO ESTUDIO 1</span></p>
                        <button class="btn-article">IR AL ARTÍCULO</button>
                    </div>
                    <div class="col-md-4 m-0 ">
                        <a href="#">
                            <img src="img/cuadri.png">
                        </a>
                        <p class="mt-3"><span>TÍTULO ESTUDIO 2</span></p>
                        <button class="btn-article">IR AL ARTÍCULO</button>
                    </div>
                    <div class="col-md-4  m-0">
                        <a href="#">
                            <img src="img/cuadri.png">
                        </a>
                        <p class="mt-3"><span>TÍTULO ESTUDIO 3</span></p>
                        <button class="btn-article">IR AL ARTÍCULO</button>
                    </div>
                </div>
            </div>

            <?php if (!checkForm_action($infoUser['id_alumno'])) {
                ?>
                <a style="text-decoration: none;" href="#">
                    <button class="btn btn-danger btn-block btn-questionnaire-home">RESPONDER AL CUESTIONARIO
                    </button>
                </a>
            <?php } ?>
        </div>
        <div class="container">
</section>

