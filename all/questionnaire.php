
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

<div class="container-fluid p-0">
    <fieldset>
        <img src="../img/esteve-top.png" alt="proyecto-opina-logo" id="logo-esteve">
        <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- The slideshow -->
            <div class="carousel-inner">
                <form method="post" name="form-questionnaire" action="?success=ok">
                    <div class="carousel-item active">
                        <input type="hidden" name="question1" value="1">
                        <p class='letter-red'>¿pregunta 1?</p>
                        <ul class="text-left">
                            <li style="list-style: none">
                                <div class="radio mb-3">
                                    <input type="radio" name="A" value="A">
                                    <span
                                            class="ml-2 letter-grey">respuesta de pregunta 1  = 1</span>
                                </div>
                            </li>
                            <li style="list-style: none">
                                <div class="radio mb-3">
                                    <input type="radio" name="A" value="B">
                                    <span
                                            class="ml-2 letter-grey">respuesta de pregunta 1  = 2</span>
                                </div>
                            </li>
                            <li style="list-style: none">
                                <div class="radio mb-3">
                                    <input type="radio" name="A" value="C">
                                    <span
                                            class="ml-2 letter-grey">respuesta de pregunta 1  = 3</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="carousel-item">
                        <input type="hidden" name="question2" value="2">
                        <p class='letter-red'>¿pregunta 2?</p>
                        <ul class="text-left">
                            <li style="list-style: none">
                                <div class="radio mb-3">
                                    <input type="radio" name="B" value="A">
                                    <span
                                            class="ml-2 letter-grey">respuesta de pregunta 2  = 1</span>
                                </div>
                            </li>
                            <li style="list-style: none">
                                <div class="radio mb-3">
                                    <input type="radio" name="B" value="B">
                                    <span
                                            class="ml-2 letter-grey">respuesta de pregunta 2  = 2</span>
                                </div>
                            </li>
                            <li style="list-style: none">
                                <div class="radio mb-3">
                                    <input type="radio" name="B" value="C">
                                    <span
                                            class="ml-2 letter-grey">respuesta de pregunta 2  = 3</span>
                                </div>
                            </li>
                            <div class="text-right pt-2">
                                <button class="btn btn-danger" type="submit" style="font-weight: bold">ENVIAR
                                </button>
                            </div>
                        </ul>
                    </div>
                </form>

                <!-- Indicators -->
                <ul class="carousel-indicators carousel-indicators-numbers">
                    <li data-target="#demo" data-slide-to="0" class='active'>1</li>
                    <li data-target="#demo" data-slide-to="1">2</li>
                </ul>
            </div>

    </fieldset>
</div>
<br>
<div id="control-carousel">
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="letter-grey"><i class="bi bi-arrow-left-circle-fill"></i> <b>Pregunta anterior</b></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="letter-grey"><b>Pregunta siguiente</b> <i class="bi bi-arrow-right-circle-fill"></i>
    </a>
</div>

<?php

