<?php



function  addInfoQuestionnaire($idUser) {
    $response_questions = [];
    array_push($response_questions, [$_POST['question1'] => $_POST['A']]);//todo question and answer 1
    array_push($response_questions, [$_POST['question2'] => $_POST['B']]);//todo question and answer 2
    $json = json_encode($response_questions);
    $SQL = 'INSERT INTO com_response_questionnaire (id_alum,response_questions) VALUES (?,?)';
    $result = Db::getInstance()->prepare($SQL);
    $result->bindParam(1, $idUser);
    $result->bindParam(2, $json);
    $result->execute();

}
 function checkForm_action($idUser)
{
    try {
        $SQL = 'SELECT * from com_response_questionnaire where id_alum=?';
        $resulta = Db::getInstance()->prepare($SQL);
        $resulta->bindParam(1, $idUser);
        $resulta->execute();
        return $resulta->rowCount();

    } catch (Exception $e) {
        die('Error al guardarse el usuario ' . $e->getMessage());
    } finally {
        $resulta = null;
    }
}

 function countInvitation($codembajador)
{
    $SQL = 'SELECT * FROM com_codigos where codigo=?';
    $result = Db::getInstance()->prepare($SQL);
    $result->bindParam(1, $codembajador);
    $result->execute();
    return $result->rowCount();
}

function infoCodigos($id){
    $SQL = 'SELECT * FROM com_codigos where id_alumno=?'; //TODO: search info codigos user
    $result = Db::getInstance()->prepare($SQL);
    $result->bindParam(1, $id);
    $result->execute();
    return $result->fetch();
}