<?php
/*
error_reporting(E_ALL);
ini_set('display_errors', '1');*/

require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';

// require_once 'lib/auth.php';
//include("cursoPDO.php");

include("function_api.php");


# Our new data

# Create a connection

if ($action == 'cemail') {

	$url = 'http://www.esteve.es/EsteveFront/RegistroExterno.do?op=CEX';
	$dev_OK = 'mensajesOK.php?status=OK&tipo=cemail';
	$dev_KO = 'micuenta_email.php?err=1';
	$fields = array();
	$fields['usu_codusuario'] = datoApi($usu_codusuario);
	$fields['usu_email'] = datoApi($usu_email);
	$fields['usu_password'] = datoApi($usu_password);
	$fields['usu_email_new'] = datoApi($usu_email_new);

	$response =  datosApi($url, $fields);

	if ($response == "KO" or empty($response)) {
		header("Location: " . $dev_KO);
	} else {
		header("Location: " . $dev_OK);
	}
} else if ($action == 'cpass') {

	$url = 'http://www.esteve.es/EsteveFront/Pwd.do?op=MCX';
	$dev_OK = 'mensajesOK.php?status=OK&tipo=pass';
	$dev_KO = 'micuenta_pass.php?err=1';
	$fields = array();
	$fields['usu_email'] = datoApi($usu_email);
	$fields['usu_password'] = datoApi($usu_password);
	$fields['usu_newpwd'] = datoApi($usu_newpwd);

	$response =  datosApi($url, $fields);

	if ($response == "KO" or empty($response)) {
		header("Location: " . $dev_KO);
	} else {
		header("Location: " . $dev_OK);
	}
} else if ($action == 'forgot') {
	$url = 'http://www.esteve.es/EsteveFront/Pwd.do?op=RCX';
	$dev_OK = 'forgot.php?status=OK';
	$dev_KO = 'forgot.php?status=KO';
	$fields = array();
	$fields['usu_email'] = datoApi($usu_email);



	$response =  datosApi($url, $fields);

	if ($response == "KO" or empty($response)) {
		header("Location: " . $dev_KO);
	} else {
		header("Location: " . $dev_OK);
	}
} else if ($action == 'registro_servicio') {
	// aqui empieza el registro en el servicio
	$url = 'http://www.esteve.es/EsteveFront/Service.do?op=AIX';
	$dev_OK = 'index.php?status=OK';
	$dev_KO = 'registro_service.php?status=KO';
	if ($mailing != 'N') {
		$mailing = "S";
	}

	$fields = array();

	$fields['con'] = datoApi($con);
	$fields['zon'] = datoApi($zon);
	$fields['usu_codusuario'] = datoApi($usu_codusuario);
	$fields['boletin_NE'] = datoApi($boletin_NE);
	$fields['usu_nombre'] = datoApi($usu_nombre);
	$fields['usu_ape1'] = datoApi($usu_ape1);
	$fields['usu_ape2'] = datoApi($usu_ape2);
	$fields['usu_email'] = datoApi($usu_email);
	$fields['usu_codperfil'] = datoApi($usu_codperfil);
	if (empty($usu_codespecialidad)) {
		$fields['usu_codespecialidad'] = 0;
	} else {
		$fields['usu_codespecialidad'] = datoApi($usu_codespecialidad);
	}
	$fields['usu_empresa'] = datoApi($usu_empresa);
	$fields['usu_codpais'] = datoApi($usu_codpais);
	$fields['usu_codprovestado'] = datoApi($usu_codprovestado);
	$fields['usu_ciudad'] = datoApi($usu_ciudad);
	$fields['mailing'] = datoApi($mailing);


	$response =  datosApi($url, $fields);

	//echo $response;

	if ($response == "KO" or empty($response)) {
		header("Location: " . $dev_KO);
	} else {
		$clave00 = uniqid();
		setcookie("clave", $clave00);

		$db = Db::getInstance();
		$data = array(
			'ape1' => $usu_ape1,
			'ape2' => $usu_ape2,
			'email' => $usu_email,
			'nombre' => $usu_nombre,
			'dni' => $usu_dni,
			'perfil' => $usu_codperfil,
			'especialidad' => $usu_codespecialidad,
			'pais' => $usu_codpais,
			'provincia' => $usu_codprovestado,
			'poblacion' => $usu_codpoblacion,
			'ciudad' => $usu_ciudad,
			'direccion' => $usu_direccion,
			'cp' => $usu_cp,
			'telefono' => $usu_telefono,
			'fax' => $usu_fax,
			'empresa' => $usu_empresa,
			'clave' => $clave00,
			'servicio' => '1'

		);
		$db->update('com_alumnos', $data, 'codusuario = :codusuario', array(':codusuario' => $usu_codusuario));




		setcookie("servicio", '1');
		header("Location: " . $dev_OK);
	}
	// aqui termina el registro en el servicio
}

// MODIFICAR LO DATOS DE USUARIO
else if ($action == 'modificar') {
	// aqui empieza el registro en el servicio
	$url = 'http://www.esteve.es/EsteveFront/RegistroExterno.do?op=MEX';
	//$dev_OK = 'registroOK.php';
	$dev_OK = 'micuenta.php?status=OK&tipo=modif';
	$dev_KO = 'micuenta.php?status=KO';


	$fields = array();


	$fields['usu_codusuario'] = datoApi($usu_codusuario);
	$fields['boletin_NE'] = datoApi($boletin_NE);
	$fields['usu_nombre'] = datoApi($usu_nombre);
	$fields['usu_ape1'] = datoApi($usu_ape1);
	$fields['usu_ape2'] = datoApi($usu_ape2);
	$fields['usu_email'] = datoApi($usu_email);
	$fields['usu_codperfil'] = datoApi($usu_codperfil);
	if (empty($usu_codespecialidad)) {
		$fields['usu_codespecialidad'] = 0;
	} else {
		$fields['usu_codespecialidad'] = datoApi($usu_codespecialidad);
	}
	$fields['usu_empresa'] = datoApi($usu_empresa);
	$fields['usu_codpais'] = datoApi($usu_codpais);
	$fields['usu_codprovestado'] = datoApi($usu_codprovestado);
	$fields['usu_ciudad'] = datoApi($usu_ciudad);
    $fields['usu_direccion'] = datoApi($usu_direccion);
    $fields['usu_telefono'] = datoApi($usu_telefono);
    $fields['usu_cp'] = datoApi($usu_cp);
    $fields['usu_empresa'] = datoApi($usu_empresa);
    $fields['usu_dni'] = datoApi($usu_dni);



	//print_r($fields);

	$response =  datosApi($url, $fields);

	//echo $response;

	if ($response == "KO" or empty($response)) {
		header("Location: " . $dev_KO);
	} else {

		$db = Db::getInstance();
		$data = array(
			'ape1' => $usu_ape1,
			'ape2' => $usu_ape2,
			'email' => $usu_email,
			'nombre' => $usu_nombre,
			'dni' => $usu_dni,
			'perfil' => $usu_codperfil,
			'especialidad' => $usu_codespecialidad,
			'numcolegiado' => $usu_numcolegiado,
			'pais' => $usu_codpais,
			'provincia' => $usu_codprovestado,
			'poblacion' => $usu_codpoblacion,
			'ciudad' => $usu_ciudad,
			'direccion' => $usu_direccion,
			'cp' => $usu_cp,
			'telefono' => $usu_telefono,
			'fax' => $usu_fax,
			'empresa' => $usu_empresa,
			'servicio' => '1'

		);
		$db->update('com_alumnos', $data, 'codusuario = :codusuario', array(':codusuario' => $usu_codusuario));


		header("Location: " . $dev_OK);
	}
	// aqui termina el registro en el servicio
}
// TERMINA MODIFICAR LOS DATOS DE USUARIO
else if ($action == 'registro') {

	// primero revisamos que el email NO exista 
	$url1 = 'http://www.esteve.es/EsteveFront/RegistroExterno.do?op=EEX';
	$fields1['usu_email'] = datoApi($usu_email);
	$response1 =  datosApi($url1, $fields1);

	if ($response1 == 'OK') {
		// SI NO EXISTE REGISTRAMOS
		$url = 'http://www.esteve.es/EsteveFront/RegistroExterno.do?op=AEX';
		$dev_OK = 'login.php?res=OK';
		$dev_KO = 'registro.php?err=1';
		// empieza el registro de usuario
		$fields['con'] = datoApi($con);
		$fields['zon'] = datoApi($zon);
		$fields['usu_password'] = datoApi($usu_password);
		$fields['boletin_NE'] = datoApi($boletin_NE);
		$fields['usu_nombre'] = datoApi($usu_nombre);
		$fields['usu_ape1'] = datoApi($usu_ape1);
		$fields['usu_ape2'] = datoApi($usu_ape2);
		$fields['usu_email'] = datoApi($usu_email);
		$fields['usu_dni'] = datoApi($usu_dni);
		$fields['usu_codperfil'] = datoApi($usu_codperfil);
		if (empty($usu_codespecialidad)) {
			$fields['usu_codespecialidad'] = 0;
		} else {
			$fields['usu_codespecialidad'] = datoApi($usu_codespecialidad);
		}
		$fields['usu_numcolegiado'] = datoApi($usu_numcolegiado);
		$fields['usu_empresa'] = datoApi($usu_empresa);
		$fields['usu_codpais'] = datoApi($usu_codpais);
		$fields['usu_codprovestado'] = datoApi($usu_codprovestado);

		$fields['usu_codpoblacion'] = datoApi($usu_codpoblacion);
		$fields['usu_ciudad'] = datoApi($usu_ciudad);
		if (empty($fields['usu_ciudad'])) {
			$fields['usu_ciudad'] = 'valor 1';
		}
		$fields['usu_direccion'] = datoApi($usu_direccion);
		$fields['usu_cp'] = datoApi($usu_cp);
		$fields['usu_telefono'] = datoApi($usu_telefono);
		$fields['usu_fax'] = datoApi($usu_dni);


		$response =  datosApi($url, $fields);

		if ($response == "KO" or empty($response)) {

			// CAMBIAR AQUI UNA VEZ SE SOLUCIONE EL PROBLEMA DEL REGISTRO
			header("Location: " . $dev_KO);
			/* foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
         rtrim($fields_string, '&');
	      echo $fields_string;*/
		} else {

			$db = Db::getInstance();
			$data = array(
				'ape1' => $fields['usu_ape1'],
				'ape2' => $fields['usu_ape2'],
				'codusuario' => $fields['usu_codusuario'],
				'email' => $fields['usu_email'],
				'nombre' => $fields['usu_nombre'],
				'dni' => $fields['usu_dni'],
				'perfil' => $fields['usu_codperfil'],
				'especialidad' => $fields['usu_codespecialidad'],
				'numcolegiado' => $fields['usu_numcolegiado'],
				'pais' => $fields['usu_codpais'],
				'provincia' => $fields['usu_codprovestado'],
				'poblacion' => $fields['usu_codpoblacion'],
				'ciudad' => $fields['usu_ciudad'],
				'direccion' => $fields['usu_direccion'],
				'cp' => $fields['usu_cp'],
				'telefono' => $fields['usu_telefono'],
				'fax' => $fields['usu_fax'],
				'empresa' => $fields['usu_empresa'],
				'usu_tipo' => $fields['usu_tipo'],
				'clave' => $clave00,
				'fecha' => $fechoy,
				'pass' => $clave02
			);
			$db->insert('com_alumnos_temp', $data);
			//$this->id = $db->lastInsertId();




			header("Location: " . $dev_OK);
		}
	} else {
		// el email ya existe en la base de datos en la plantilla login debe mostrar error de email existente
		header("Location: registro.php?err=6");
	}
	// termina el registro de usuario
}
