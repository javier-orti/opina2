<?php 

    require_once '../lib/autoloader.class.php';

    require_once '../lib/init.class.php';

    require_once '../lib/authAdmin.php';

    session_start();

    $sesion = $_COOKIE["evento_id"];

 $prox = New Evento();

    $prox->getOne($sesion);

    $eventos = $prox->row[0];

    $preguntas = $prox->getPreguntaPonente($sesion);
	
	foreach($preguntas as $pregunta) { ?>

            <div class="col pregunta">

                <h2><?php echo $pregunta['nombre'].' '.$pregunta['ape1']; ?></h2>

                <div class="row">

                    <div class="col-9">  

                        <p><?php echo $pregunta['pregunta']; ?></p>

                    </div>

                    <div class="col-3 text-center heart">

                       <a id="<?php echo $pregunta['id']; ?>" rel="<?php if ($pregunta['favorito'] == 0) {echo '1';} else {echo '0';} ?>" class="click_heart"><span id="h_<?php echo $pregunta['id']; ?>" class="material-icons">favorite<?php if ($pregunta['favorito'] == 0) echo '_border'; ?></span></a>

                    </div>

                </div>

            </div>

        <? } ?>
			<script>
		$(document).ready(function($)
    {	
		$(".click_heart").click(function(e) {


                    console.log("se envia el form");
					var theName = $(this).attr("id");
                  var url = "change.php";
                  console.log($("#pregunta").serialize());
                  $.ajax({
                    type: "POST",
                    url: url,
                    data: {"fav": $('[id$='+theName+']').attr('rel'), "id": $('[id$='+theName+']').attr('id')},
                    success: function(data)
                    {
                      //var result = $.parseJSON(data);
                     
					  var cambio = $('[id$='+theName+']').attr('rel');
					  if (cambio == '1') {
						cambio = '0';						
						texto = "favorite";
						
                      } else {
						cambio = '1';
						texto = "favorite_border";
                      }
          
                      if (data == 'ok') {
						$('#'+theName).attr('rel', cambio);
						$('#h_'+theName).text(texto);

                      }

                    }
                  });

                  e.preventDefault(); // avoid to execute the actual submit of the form.
                });
 });
</script>