<?php 

    require_once '../lib/autoloader.class.php';

    require_once '../lib/init.class.php';

    require_once '../lib/authAdmin.php';

    session_start();

    $sesion = $_COOKIE["evento_id"];

    $prox = New Evento();

    $prox->getOne($sesion);

    $eventos = $prox->row[0];

    $preguntas = $prox->getPreguntaPonenteFav($sesion);

?>

<!doctype html>

<html lang="es">

  <head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/estilos.css" >

    <title>Panel de preguntas</title>

  </head>

  <body>

    <div class="logo">

        <b>Canal SNC - Panel de preguntas</b>

    </div>

    <h1>PREGUNTAS FAVORITAS</h1>

    <br>

    <div class="container">

        <div class="row row-cols-1 preguntas">

        <?php foreach($preguntas as $pregunta) { ?>

            <div class="col pregunta">

                <h2><?php echo $pregunta['nombre'].' '.$pregunta['ape1']; ?></h2>

                <div class="row">

                    <div class="col">  

                        <p><?php echo $pregunta['pregunta']; ?></p>

                    </div>

                    

                </div>

            </div>

        <? } ?>

            

            

        </div>

    </div>

    <nav class="bottom-nav">

        <a href="intro.php">TODAS</a>

        <a href="favoritas.php" class="selected">FAVORITAS</a>

    </nav>

    

    



    <!-- Optional JavaScript -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  </body>

</html>