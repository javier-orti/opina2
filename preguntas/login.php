<?php 

error_reporting(E_ALL);
ini_set('display_errors', '1');


 require_once '../lib/autoloader.class.php';

    require_once '../lib/init.class.php';



    if (isset($_GET['sesion'])){

        $prox = New Evento();
        $eleve = $prox->getOnebyCod($_GET['sesion']);
        $eventos = $eleve[0];
        $id = $eventos['id'];

        

    } else {

        echo '<p>No se ha especificado el código del evento</p>';

        die();

    }
   

    $URL = '/preguntas/';

?>  

<!doctype html>

<html lang="es">

  <head><meta charset="gb18030">

    <!-- Required meta tags -->

    

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo $URL;?>css/estilos.css" >

    <title>Panel de preguntas</title>

  </head>

  <body>

    <div class="container">

        <div class="logo">

            <b>Canal SNC - Panel de preguntas</b>

        </div>

        <div class="login">

            <form action="<?php echo $URL;?>action-login.php" method="post">
                <input type="hidden" value="<?php echo $id;?>" name="evento">

                <p>Usuario:</p>

                <input type="text" class="form-control" name="usu_email">

                <br>

                <p>Contraseña:</p>

                <input type="password" class="form-control" name="usu_password">

                <br>

                <br>

                <button type="submit" class="btn btn-primary btn-block">ACCEDER</button>

            </form>

        </div>

        

    </div>

    

    

    

    



    <!-- Optional JavaScript -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  </body>

</html>