<?php 

    require_once '../lib/autoloader.class.php';

    require_once '../lib/init.class.php';

    require_once '../lib/authAdmin.php';

    session_start();

    //echo "evento_id: ".$_COOKIE["evento_id"];

   
        $sesion = $_COOKIE["evento_id"];
   

      

    

    ?>

<!doctype html>

<html lang="es">

  <head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/estilos.css" >
	 <script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
	
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	

    <title>Panel de preguntas</title>

  </head>

  <body>

    <div class="logo">

        <b>Canal SNC - Panel de preguntas</b>

    </div>

    <h1>PREGUNTAS AUDIENCIA</h1>

    <br>

    <div class="container">

        <div id="load_preg" class="row row-cols-1 preguntas">

        
              <?php include('load_preg.php'); ?>

        </div>

    </div>

    <nav class="bottom-nav">

        <a href="intro.php" class="selected">TODAS</a>

        <a href="favoritas.php">FAVORITAS</a>

    </nav>

    

    



    <!-- Optional JavaScript -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

   
	<script>
	
		$(document).ready(function($)
    {		
				
				function loadPreg() {
					//console.log("entra");
					//$("#ponente_preguntas").load("ponente_preguntas.php?id=<?php echo $id;?>");
					 var url = "load_preg.php";
					$.ajax({
                    type: "POST",
                    url: url,
					// data: {"fav": $('[id$='+theName+']').attr('rel'), "id": $('[id$='+theName+']').attr('id')},

                    success: function(data)
                    {


                            $("#load_preg").html(data);

							setTimeout(loadPreg, 10000);



                    }
                  });


				}
				setTimeout(loadPreg, 10000);
			   });	
	</script>
				
  </body>

</html>