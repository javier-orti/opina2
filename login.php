<?php
error_reporting(E_ALL);
ini_set('display_errors', '0');
$act = '';
$reg = '';
$res = '';
$recpass = '';
$err = '';
require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';
require_once 'lib/auth_off.php';
$page = 'login';
session_start();
if ($_GET['codigo']) {
    $_SESSION['code'] = $_GET['codigo'];
} else {
    session_unset();
}

include('header.php');
?>
<section id="login-opina">
    <div class="container py-4">
        <div class="row">
            <div class="col-xs-12 col-lg-2"></div>
            <div class="col-xs-12 col-lg-8">
                <p class="letter-grey">Para poder acceder al contenido, inicia sesión o crea una cuenta.<br>Si ya estás
                    dado de alta en
                    ESTEVE
                    introduce tu usuario y contraseña.
                </p>
                <?php if ($act == 'OK' or $act == 'rOK' or $res == 'OK') { ?>
                    <div class="alert alert-success">Gracias por registrarse, debe confirmar su registro. <br>Revise la
                        bandeja de entrada de su correo electrónico y haga clic en el enlace que aparece en el email que
                        le
                        hemos enviado.<br>En caso de que no aparezca en su bandeja principal , revise en la carpeta de
                        "Correo no deseado".<br><br></div>
                <?php } else if ($reg == 'OK') { ?>
                    <div class="alert alert-success">Gracias por registrarse, a partir de ahora puede acceder a nuestro
                        servicio introduciendo sus datos de acceso.
                    </div>
                <?php } else if ($recpass == 'OK') { ?>
                    <div class="alert alert-success">Se ha guardado su nueva contraseña, puede acceder a su cuenta.
                    </div>
                <?php } else if ($err == '2' or $err == '1') { ?>
                    <div class="alert alert-danger">El correo y la contraseña no coinciden.</div>
                <?php } else if ($err == '3') { ?>
                    <div class="alert alert-success">Su cuenta ha sido activada previamente, ya puede acceder con su
                        email y
                        contraseña.
                    </div>
                <?php } else if ($err == '4') { ?>
                    <div class="alert alert-danger">Debe activar su cuenta antes de acceder usando el email que recibió
                        en
                        su correo electrónico en el momento del registro.
                    </div>
                <?php } else if ($err == '6') { ?>
                    <div class="alert alert-danger">El email que está intentado registrar ya está registrado en la base
                        de
                        datos, use su correo y contraseña para acceder o recupere su contraseña si la ha olvidado.
                    </div>
                <?php } else if ($err == '7') { ?>
                    <div class="alert alert-danger">Su identidad no ha sido verificada aún.<br> Revise la bandeja de
                        entrada
                        de su correo electrónico y haga clic en el enlace que aparece en el email que le hemos
                        enviado.<br>
                        En caso de que no aparezca en su bandeja principal, revise en la carpeta de "Correo no deseado".<br>
                        Si no recibió el correo de confirmación <a
                                href="confirmacion_email.php?id=<?php echo $id; ?>&uniqueid=<?php echo $uniqueid; ?>">haga
                            click aqui para volver a enviarlo</a>. <br><br></div>
                <?php } else if ($err == '8') { ?>
                    <div class="alert alert-danger">No se ha podido enviar el email. <br><br></div>

                <?php } ?>
                <form action="action_login.php" name='login' method="post">
                    <div class="row ">
                        <div class="col-10">
                            <p class=" letter-red">Correo electrónico</p>
                            <input type="email" class="form-control" name="usu_email" placeholder="Correo electrónico *" required>
                        </div>
                        <div class="col-10 mt-3 ">
                            <p class="letter-red ">Contraseña</p>
                            <input type="password" class="form-control" name="usu_password" placeholder="Contraseña *" required>
                            <div class="text-right">
                            <p ><a href="#" class="letter-grey" id="recover-pass">¿Has olvidado la contraseña?</a></p>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="text-left  col-12">
                            <button class="btn btn-danger mb-3 pl-4 pr-4"
                                    type="submit"><i class="bi bi-arrow-right-circle"></i> INICIAR SESIÓN
                            </button>
                        </div>
                        <div class="text-left col-12 ">
                            <a href="#" class="btn btn-light pl-4 pr-4" id="btn-new-user">
                                <i class="bi bi-arrow-right-circle"></i> CREAR CUENTA
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 col-lg-2"></div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>
