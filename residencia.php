<?php
error_reporting(E_ALL);
ini_set('display_errors', '0');
require_once 'lib/autoloader.class.php';
require_once 'lib/init.class.php';
require_once 'lib/auth.php';
$page = 'codigo';
include('header.php');

?>
    <div class="contenido">
        <div class="container">
            <div class="my-4">
                <div class="title text-center">
                    <p class="letter-red">Introduce tu año de residencia:</p>
                    <form action="agregar_residencia.php" method="post">
                        <select name="ano_residencia" id="residencia" class="form-control my-4"
                                style="max-width:400px;margin:0 auto">
                            <?php
                            for ($i = 1950; $i <= 2022; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            }
                            ?>
                            <option value="0000">En curso</option>
                        </select>
                        <div class="text-center">
                            <button type="submit" class="pr-5 pl-5 pt-2 pb-2" id="btn-code-verificate">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php'); ?>